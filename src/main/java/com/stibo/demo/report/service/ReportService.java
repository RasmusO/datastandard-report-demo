package com.stibo.demo.report.service;

import com.stibo.demo.report.logging.LogTime;
import com.stibo.demo.report.model.*;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Service
public class ReportService {

    final static List<String> HEADERS = Arrays.asList("Category", "Attribute", "Description", "Type", "Groups");
    final static String INDENT = "  ";

    static String getType(final Datastandard datastandard, final Attribute attribute, final String indent) {
        final AttributeType attributeType = attribute.getType();
        return attributeType.getId() +
                (isTypeRecursive(attribute) ? "{\n" + String.join("\n", getInnerType(datastandard, attribute, new ArrayList<>(), indent)) : "") +
                (attributeType.getMultiValue() ? "[]" : "");
    }

    static boolean isTypeRecursive(final Attribute attribute) {
        return attribute.getAttributeLinks() != null && !attribute.getAttributeLinks().isEmpty();
    }

    static List<String> getInnerType(final Datastandard datastandard, final Attribute parentAttribute, final List<String> parents, final String parentIndent) {
        final String childIndent = parentIndent + INDENT;
        parentAttribute.getAttributeLinks().forEach(attributeLink -> getAttributes(datastandard, attributeLink).forEach(attribute -> parents.add(childIndent + getAttributeName(attribute, attributeLink) + ": " + getType(datastandard, attribute, childIndent))));
        parents.add(parentIndent + "}");
        return parents;
    }

    static String getAttributeName(final Attribute attribute, final AttributeLink attributeLink) {
        return attribute.getName() + (attributeLink.getOptional() ? "" : "*");
    }

    static Stream<Category> getCategories(final Datastandard datastandard, String categoryId) {
        return datastandard.getCategories().stream().filter(category -> category.getId().equals(categoryId));
    }

    static Stream<Attribute> getAttributes(final Datastandard datastandard, final AttributeLink attributeLink) {
        return datastandard.getAttributes().stream().filter(attribute -> attribute.getId().equals(attributeLink.getId()));
    }

    static String getGroups(final Datastandard datastandard, final Attribute attribute) {
        final List<String> groups = new ArrayList<>();
        attribute.getGroupIds().forEach(groupId -> getAttributeGroups(datastandard, groupId).forEach(attributeGroup -> groups.add(attributeGroup.getName()))
        );
        return String.join("\n", groups);
    }

    static Stream<AttributeGroup> getAttributeGroups(final Datastandard datastandard, final String groupId) {
        return datastandard.getAttributeGroups().stream().filter(attributeGroup -> attributeGroup.getId().equals(groupId));
    }

    static void handleCategories(final Datastandard datastandard, final String categoryId, final List<Stream<String>> rows) {
        getCategories(datastandard, categoryId).forEach(category -> {
            final String parentId = category.getParentId();
            if (parentId != null && !parentId.isEmpty()) {
                handleCategories(datastandard, parentId, rows);
            }

            category.getAttributeLinks().forEach(attributeLink -> getAttributes(datastandard, attributeLink).forEach(attribute -> {
                final List<String> columns = new ArrayList<>();
                columns.add(category.getName());
                columns.add(getAttributeName(attribute, attributeLink));
                columns.add(attribute.getDescription());
                columns.add(getType(datastandard, attribute, ""));
                columns.add(getGroups(datastandard, attribute));
                rows.add(columns.stream());
            }));
        });
    }

    @LogTime
    public Stream<Stream<String>> report(final Datastandard datastandard, final String categoryId) {
        final List<Stream<String>> rows = new ArrayList<>();
        rows.add(HEADERS.stream());
        handleCategories(datastandard, categoryId, rows);
        return rows.stream();
    }
}
